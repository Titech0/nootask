from flask import Flask
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy

import connexion

# Create the application instance
app = Flask(__name__)
api = connexion.App(__name__, specification_dir='./')
# Read the swagger.yml file to configure the endpoints
api.add_api('swagger.yml')

# database
app.config['SQLALCHEMY_DATABASE_URL'] = 'sqllite:///infusionlite.db'
db = SQLAlchemy


class Beacon(db.Model):
    # Mac address of beacon
    mac_address = db.Column(db.String(18), primary_key=True, nullable=False)
    # Beacon broadcast name
    beacon_name = db.Column(db.String(10), primary_key=True, nullable=False)
    # Beacon battery charge level
    charge_level = db.Column(db.Integer)
    # Department identifier
    department_id = db.Column(db.Integer)
    # Location identifier
    location_id = db.Column(db.Integer)


@app.route('/')
def index():
    return "Test"


if __name__ == "__main__":
    app.run(debug=True)
